import React from 'react';
import Link from 'next/link';

interface IButton {
  children: string;
  className?: string;
  href?: string;
  active?: string;
  onClick?: () => void;
}

const Button = ({ children, className, href, active, onClick }: IButton) => {
  if (href) {
    return (
      <Link href={href}>
        <a className={className} style={{ textDecoration: 'none' }}>
          {children}
        </a>
      </Link>
    );
  }
  return (
    <button
      className={`${className} ${active ? active : ''}`}
      onClick={onClick}
    >
      {children}
    </button>
  );
};

export default Button;
