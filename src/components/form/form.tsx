import React from 'react';
import Input from '../input/input';
import IForm from '../../interfaces/form/form.interface';
import Button from '../button/button';

const Form = ({
  inputs,
  btnLabel,
  register,
  handleSubmit,
  onSubmit,
  errors,
  inputsClassName,
  btnClassName,
}: IForm) => {
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      {inputs.map((input, i) => (
        <Input
          key={i}
          type={input.type}
          name={input.name}
          placeholder={input.placeholder}
          error={errors[input.name]?.message}
          register={register}
          inputsClassName={inputsClassName}
        />
      ))}
      <Button className={btnClassName}>{btnLabel}</Button>
    </form>
  );
};

export default Form;
