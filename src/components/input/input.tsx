import React from 'react';
import IInput from '../../interfaces/input/input.interface';

const Input = ({
  type,
  placeholder,
  name,
  register,
  error,
  inputsClassName,
}: IInput) => {
  return (
    <div>
      <input
        type={type}
        placeholder={placeholder}
        {...register(name)}
        className={inputsClassName ? inputsClassName : ''}
      />
      {error && <p>{error}</p>}
    </div>
  );
};

export default Input;
