import React from 'react';
import styles from './Card.module.scss';

const Card = ({ title, description, color }) => {
  return (
    <div
      className={`${styles.Card} ${color === 'red' ? styles.Card__red : ''} ${
        color === 'green' ? styles.Card__green : ''
      }`}
    >
      <h2>{title}</h2>
      <p>{description}</p>
    </div>
  );
};

export default Card;
