import React, { useEffect } from 'react';
import * as d3 from 'd3';
import styles from './PieChart.module.scss';

const PieChart = ({ data }) => {
  const width = 450;
  const height = 350;
  const radius = Math.min(width, height) / 2;

  useEffect(() => {
    if (!data) return;
    // append the svg object to the div called 'my_dataviz'
    const svg = d3
      .select('#my_dataviz')
      .append('svg')
      .attr('width', width)
      .attr('height', height)
      .append('g')
      .attr('transform', `translate(${width / 2}, ${height / 2})`);

    // Color
    const color = d3.scaleOrdinal([
      '#9DC6E1',
      '#79B0D6',
      '#549BCB',
      '#2F85C0',
      '#0B70B5',
    ]);

    // Compute the position of each group on the pie:
    const pie = d3
      .pie()
      .sort(null)
      .value((d) => {
        return d[1];
      });

    const dataReady = pie(data);

    // shape helper to build arcs:
    const arcGenerator = d3.arc().innerRadius(0).outerRadius(radius);

    // Build the pie chart
    svg
      .selectAll('mySlices')
      .data(dataReady)
      .join('path')
      .attr('d', arcGenerator)
      .attr('fill', (d) => color(d.data[0]));

    // add the annotation
    svg
      .selectAll('mySlices')
      .data(dataReady)
      .join('text')
      .text((d) => `${d.data[0]} : ${d.data[1]}`)
      .attr('transform', (d) => `translate(${arcGenerator.centroid(d)})`)
      .style('text-anchor', 'middle')
      .style('font-size', 15)
      .style('font-weight', 'bold')
      .style('fill', '#fff');

    return () => {
      document.getElementById('my_dataviz').innerHTML = '';
    };
  }, [data]);

  return (
    <div className={styles.PiedChart}>
      <h2>Top 5 des produits vendus :</h2>
      <div id="my_dataviz"></div>
    </div>
  );
};

export default PieChart;
