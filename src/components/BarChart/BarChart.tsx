import React, { useEffect } from 'react';
import * as d3 from 'd3';
import styles from './BarChart.module.scss';

const BarChart = ({ stats }) => {
  useEffect(() => {
    console.log(stats, 'stats');

    const max = stats.reduce((acc, stat) => {
      if (stat.value > acc) {
        acc = stat.value;
      }
      return acc;
    }, 0);

    const margin = 40;
    const width = 450;
    const height = 335;

    const svg = d3.select('#barchart');
    const chart = svg
      .append('g')
      .attr('transform', `translate(${margin}, ${0})`);

    const yScale = d3
      .scaleLinear()
      .range([height, 0])
      .domain([0, max + 10]);
    chart.append('g').call(d3.axisLeft(yScale));

    const xScale = d3
      .scaleBand()
      .range([0, width])
      .domain(stats.map((s) => s.period))
      .padding(0.2);

    chart
      .append('g')
      .attr('transform', `translate(0, ${height})`)
      .call(d3.axisBottom(xScale));

    chart
      .append('g')
      .attr('class', 'grid')
      .call(d3.axisLeft().scale(yScale).tickSize(-width, 0, 0).tickFormat(''));

    // draw chart
    const barGroups = chart.selectAll().data(stats).enter().append('g');
    barGroups
      .append('rect')
      .attr('class', 'bar')
      .attr(
        'x',
        (g) =>
          xScale(g.period) + (xScale.bandwidth() - xScale.bandwidth() / 1.5) / 2
      )
      .attr('y', (g) => yScale(g.value))
      .attr('height', (g) => height - yScale(g.value))
      .attr('width', xScale.bandwidth() / 1.5)
      .style('fill', '#0B70B5')
      .style('opacity', '0.7');

    barGroups
      .append('text')
      .attr('class', 'value')
      .attr('x', (a) => xScale(a.period) + xScale.bandwidth() / 2)
      .attr('y', (a) => yScale(a.value) + 10)
      .attr('text-anchor', 'middle')
      .text((a) => `${a.value}`)
      .style('font-size', 10)
      .style('fill', '#fff');

    return () => {
      svg.selectAll('*').remove();
    };
  }, [stats]);

  return (
    <div className={styles.BarChart}>
      <h2>Evolution du chiffre d'affaire :</h2>
      <svg id="barchart" style={{ width: '500px', height: '400px' }}></svg>
    </div>
  );
};

export default BarChart;
