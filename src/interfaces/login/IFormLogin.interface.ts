interface IFormLogin {
  email: string;
  password: string;
}

export default IFormLogin;
