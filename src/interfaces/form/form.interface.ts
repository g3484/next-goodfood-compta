import { UseFormRegister } from 'react-hook-form';

interface IForm {
  inputs: {
    type: string;
    placeholder: string;
    name: any;
  }[];
  btnLabel: string;
  register: UseFormRegister<any>;
  handleSubmit: Function;
  onSubmit: Function;
  errors: Object;
  inputsClassName?: string;
  btnClassName?: string;
}

export default IForm;
