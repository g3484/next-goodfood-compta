import { Path, UseFormRegister } from 'react-hook-form';
import IFormLogin from '../login/IFormLogin.interface';

interface InputInterface {
  type: string;
  name: Path<IFormLogin>;
  placeholder: string;
  register: UseFormRegister<IFormLogin>;
  error: string;
  inputsClassName?: string;
}

export default InputInterface;
