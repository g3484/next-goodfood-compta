import React from 'react';
import Head from 'next/head';
import Layout from '../components/layout';
import LoginContainer from '../containers/Login/LoginContainer';

const login = () => {
  return (
    <Layout>
      <Head>
        <title>Login - Goodfood</title>
      </Head>

      <LoginContainer />
    </Layout>
  );
};

export default login;
