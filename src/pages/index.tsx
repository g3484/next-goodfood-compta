import React from 'react';
import Head from 'next/head';
import Layout from '../components/layout';
import Stats from '../containers/Stats/Stats';

const Home = () => {
  return (
    <Layout>
      <Head>
        <title>Goodfood - Compta</title>
      </Head>

      <Stats />
    </Layout>
  );
};

export default Home;
