import React, { useEffect, useState } from 'react';
import Card from '../../../components/Card/Card';
import { getCountOrdersCancelled } from '../../../services/axios/statsRequest';
import { useRouter } from 'next/router';

const Orders = () => {
  const [cancelledOrdersNb, setCancelledOrdersNb] = useState(0);
  const router = useRouter();

  const fetchCancelledOrdersNb = async () => {
    const { data, status } = await getCountOrdersCancelled(
      '/api/statistics/cancelled'
    );

    if (status === 200) {
      const parsedData = JSON.parse(data.nbOrders);
      return setCancelledOrdersNb(parsedData.count);
    }

    if (status === 401) {
      return router.push('/login');
    }

    if (status !== 200) {
      alert('Une erreur est survenue');
    }
  };

  useEffect(() => {
    fetchCancelledOrdersNb();
  }, []);
  return (
    <Card
      title={'Commandes annulées'}
      description={`Le nombre de commandes ayant un statut annulée est de : ${cancelledOrdersNb}`}
      color="red"
    />
  );
};

export default Orders;
