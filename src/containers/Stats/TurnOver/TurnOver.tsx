import React, { useEffect, useState } from 'react';
import BarChart from '../../../components/BarChart/BarChart';
import DatePicker from 'react-date-picker/dist/entry.nostyle';
import { getTurnOver } from '../../../services/axios/statsRequest';
import { useRouter } from 'next/router';
import styles from './TurnOver.module.scss';
import dayjs from 'dayjs';

const TurnOver = () => {
  const [date, setDate] = useState(new Date());
  const [period, setPeriod] = useState('month');
  const [data, setData] = useState([]);

  const router = useRouter();

  const fetchTurnover = async (date, period) => {
    const { data, status } = await getTurnOver('/api/statistics/turnover', {
      date,
      period,
    });

    if (status === 200) {
      const parsedData = JSON.parse(data.turnover);
      return parsedData.turnover;
    }

    if (status === 401) {
      return router.push('/login');
    }

    if (status !== 200) {
      alert('Une erreur est survenue');
    }
  };

  const getLastSixElements = async (date, period) => {
    const elements = [];
    for (let i = 0; i < 6; i++) {
      const newDate = dayjs(date).subtract(i, period).format('YYYY-MM-DD');

      const turnover = await fetchTurnover(newDate, period);

      let periodName = '';

      if (period === 'day') {
        periodName = dayjs(newDate).format('DD/MM/YYYY');
      }

      if (period === 'week') {
        periodName = dayjs(newDate).format('DD/MM/YYYY');
      }

      if (period === 'month') {
        periodName = dayjs(newDate).format('MMMM');
      }

      if (period === 'year') {
        periodName = dayjs(newDate).format('YYYY');
      }

      elements.push({
        period: periodName,
        value: turnover,
      });
    }

    return setData(elements.reverse());
  };

  useEffect(() => {
    if (!date) return;
    if (!period) return;

    console.log(date, 'turnover date');

    getLastSixElements(date, period);
  }, [date, period]);

  return (
    <div className={styles.TurnOver}>
      <div className={styles.TurnOver__date}>
        <DatePicker onChange={setDate} value={date} />

        <select
          onChange={(e) => setPeriod(e.target.value)}
          defaultValue={'month'}
        >
          <option value="day">Jour</option>
          <option value="week">Semaine</option>
          <option value="month">Mois</option>
          <option value="year">Année</option>
        </select>
      </div>
      <BarChart stats={data} />
    </div>
  );
};

export default TurnOver;
