import React, { useEffect, useState } from 'react';
import Card from '../../../components/Card/Card';
import { getCountDeliveries } from '../../../services/axios/statsRequest';
import { useRouter } from 'next/router';

const Deliveries = () => {
  const [deliveriesNb, setDeliveriesNb] = useState(0);
  const router = useRouter();

  const fetchDeliveries = async () => {
    const { data, status } = await getCountDeliveries(
      '/api/statistics/deliveries'
    );

    if (status === 200) {
      const parsedData = JSON.parse(data.nbDeliveries);
      return setDeliveriesNb(parsedData.count);
    }

    if (status === 401) {
      return router.push('/login');
    }

    if (status !== 200) {
      alert('Une erreur est survenue');
    }
  };

  useEffect(() => {
    fetchDeliveries();
  }, []);

  return (
    <Card
      title={'Livraisons effectués'}
      description={`Le nombre de livraison effectuée est de : ${deliveriesNb}`}
      color="green"
    />
  );
};

export default Deliveries;
