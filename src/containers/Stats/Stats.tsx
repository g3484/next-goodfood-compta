import React from 'react';
import Products from './Products/Products';
import TurnOver from './TurnOver/TurnOver';
import styles from './Stats.module.scss';
import Orders from './Orders/Orders';
import Deliveries from './Deliveries/Deliveries';

const Stats = () => {
  return (
    <div className={styles.Stats}>
      <div className={styles.Stats__content}>
        <Products />
        <TurnOver />
        <Orders />
        <Deliveries />

        <div className={styles.Stats__content__disconnect}></div>
      </div>
    </div>
  );
};

export default Stats;
