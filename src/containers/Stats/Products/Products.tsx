import React, { useEffect, useState } from 'react';
import PieChart from '../../../components/PieChart/PieChart';
import DatePicker from 'react-date-picker/dist/entry.nostyle';
import { getTopProducts } from '../../../services/axios/statsRequest';
import { useRouter } from 'next/router';
import styles from './Products.module.scss';
import dayjs from 'dayjs';

const Products = () => {
  const [date, setDate] = useState(new Date(dayjs().format('YYYY-MM-DD')));
  const [period, setPeriod] = useState('month');
  const [data, setData] = useState([]);

  const router = useRouter();

  const fetchTopProducts = async (date, period) => {
    const newDate = dayjs(date).format('YYYY-MM-DD');

    const { data, status } = await getTopProducts(
      '/api/statistics/topProduct',
      {
        date: newDate,
        period,
      }
    );

    if (status === 200) {
      const parsedData = JSON.parse(data.products);

      console.log(parsedData, 'parsed');

      const formattedData = parsedData.map((product) => [
        product.name,
        product.count,
      ]);

      return setData(formattedData);
    }

    if (status === 401) {
      return router.push('/login');
    }

    if (status !== 200) {
      alert('Une erreur est survenue');
    }
  };

  useEffect(() => {
    if (!date) return;
    if (!period) return;

    fetchTopProducts(date, period);
  }, [date, period]);

  useEffect(() => {
    const data = {
      Produits: 1,
      Produits2: 1,
      Produits3: 1,
      Produits4: 1,
      Produits5: 1,
    };
    console.log(Object.entries(data));
  }, []);

  return (
    <div className={styles.Products}>
      <div className={styles.Products__date}>
        <DatePicker onChange={setDate} value={date} />

        <select
          onChange={(e) => setPeriod(e.target.value)}
          defaultValue={'month'}
        >
          <option value="day">Jour</option>
          <option value="week">Semaine</option>
          <option value="month">Mois</option>
          <option value="year">Année</option>
        </select>
      </div>
      <PieChart data={data} />
    </div>
  );
};

export default Products;
