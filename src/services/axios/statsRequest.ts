import axiosClient from './axiosClient';

/** LOGIN REQUEST */

export const getTurnOver = async (URL: string, params: object) => {
  return await axiosClient
    .get(URL, {
      params,
    })
    .then((response) => response)
    .catch((error) => error.response);
};

export const getTopProducts = async (URL: string, params: object) => {
  return await axiosClient
    .get(URL, {
      params,
    })
    .then((response) => response)
    .catch((error) => error.response);
};

export const getCountOrdersCancelled = async (URL: string) => {
  return await axiosClient
    .get(URL)
    .then((response) => response)
    .catch((error) => error.response);
};

export const getCountDeliveries = async (URL: string) => {
  return await axiosClient
    .get(URL)
    .then((response) => response)
    .catch((error) => error.response);
};
