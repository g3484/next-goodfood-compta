/* eslint-disable camelcase */
import axios from 'axios';
import jwtDecode from 'jwt-decode';
import dayjs from 'dayjs';

interface TokenDto {
  id: string;
  email: string;
  exp: number;
  iat: number;
}

const axiosClient = axios.create();

if (typeof window === 'undefined') {
  axiosClient.defaults.baseURL =
    'http://ingress-nginx-controller.ingress-nginx.svc.cluster.local';

  axiosClient.defaults.headers.common['Host'] = 'goodfood.sbs';
} else {
  axiosClient.defaults.baseURL = '/';
}

axiosClient.defaults.withCredentials = true;

axiosClient.interceptors.request.use(async (config) => {
  if (typeof window === 'undefined') return config;

  const accessToken = localStorage.getItem('access_token');
  const refreshToken = localStorage.getItem('refresh_token');

  if (!accessToken || !refreshToken) return config;

  config.headers['Authorization'] = `Bearer ${accessToken}`;

  try {
    const user = jwtDecode(accessToken) as TokenDto;
    const isExpired = dayjs().isAfter(dayjs.unix(user.exp));

    if (!isExpired) return config;

    const response = await axios.post(
      `${process.env.API_ENDPOINT}/refresh_token`,
      {
        refreshToken: refreshToken,
      }
    );

    const { access_token, refresh_token } = response.data;

    localStorage.setItem('access_token', access_token);
    localStorage.setItem('refresh_token', refresh_token);

    config.headers['Authorization'] = `Bearer ${access_token}`;
    return config;
  } catch (error) {
    console.log(error);
    return config;
  }
});

export default axiosClient;
