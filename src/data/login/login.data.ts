import { Path } from 'react-hook-form';
import IFormLogin from '../../interfaces/login/IFormLogin.interface';

export const inputLoginData = [
  {
    type: 'email',
    placeholder: 'Email',
    name: 'email' as Path<IFormLogin>,
  },
  {
    type: 'password',
    placeholder: 'Password',
    name: 'password' as Path<IFormLogin>,
  },
];
